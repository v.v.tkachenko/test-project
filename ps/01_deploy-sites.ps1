$serverlist = "3.121.206.135", "35.159.11.97"       # List of server from AWS
$SiteFolderPath = "c:\inetpub\wwwroot\"              # Website Folder
$SiteAppPool = "DefaultAppPool"                      # Application Pool Name
$SiteName = "Default Web Site"                       # IIS Site Name
$RDPuser="demo"



$cred= get-credential $RDPuser

foreach ($server in $serverlist) {
Invoke-Command -ComputerName $server  -Credential $cred   -ScriptBlock { Get-ChildItem -Path $Using:SiteFolderPath -Include * -File -Recurse | foreach { $_.Delete()}}
Invoke-Command -ComputerName $server  -Credential $cred    -ScriptBlock {remove-Website -Name $Using:SiteName }    
Invoke-Command -ComputerName $server  -Credential $cred    -ScriptBlock {  Remove-WebAppPool -Name  $Using:SiteAppPool}    
Invoke-Command -ComputerName $server  -Credential $cred   -ScriptBlock {Set-Content $Using:SiteFolderPath'Default.htm' "<h1>Demo $Using:server</h1>" }  
Invoke-Command -ComputerName $server  -Credential $cred   -ScriptBlock {New-WebAppPool -Name $Using:SiteAppPool}
Invoke-Command -ComputerName $server  -Credential $cred   -ScriptBlock { Start-IISCommitDelay; $TestSite =New-IISSite -Name $Using:SiteName  -BindingInformation "*:80:" -PhysicalPath  $Using:SiteFolderPath   -Passthru; $TestSite.Applications["/"].ApplicationPoolName = $using:SiteAppPool;  Stop-IISCommitDelay; Start-WebSite -Name $Using:SiteName} 
}
