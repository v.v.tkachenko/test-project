 resource "aws_lb" "test" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.main-public-1.id,aws_subnet.main-public-2.id]
  enable_cross_zone_load_balancing =true
#  enable_deletion_protection = true

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group_attachment" "vm1" {
  target_group_arn = aws_lb_target_group.test.arn
  target_id        = aws_instance.vm1.id
  port             = 80
}
resource "aws_lb_target_group_attachment" "vm2" {
  target_group_arn = aws_lb_target_group.test.arn
  target_id        = aws_instance.vm2.id
  port             = 80
}


resource "aws_lb_target_group" "test" {
   name     = "tf-example-lb-tg"
  port     = 80
  protocol = "TCP"
  vpc_id   = aws_vpc.main.id
  target_type = "instance"

  }

resource "aws_lb_listener" "test" {
  load_balancer_arn = aws_lb.test.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test.arn
  }
}



resource "aws_security_group" "demo" {
  name        = "Demo security group"
  description = "allow http, winrm"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "80  from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 ingress {
    description = "HTTP wirnm"
    from_port   = 5985
    to_port     = 5985
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

ingress {
    description = "HTTPS wirnm"
    from_port   = 5986
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

ingress {
    description = "RDP"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 ingress {
    description = "icmp"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow http, Winrm"
  }
}




resource "aws_instance" "vm1" {
  ami           = data.aws_ami.windows-ami.image_id
  instance_type = "t2.micro"
# key_name      = aws_key_pair.mykey.key_name
#  count         =  var.INSTANCE_COUNT
 subnet_id = aws_subnet.main-public-1.id
 user_data     = <<EOF
<powershell>
net user ${var.INSTANCE_USERNAME} '${var.INSTANCE_PASSWORD}' /add /y
net localgroup administrators ${var.INSTANCE_USERNAME} /add

winrm quickconfig -q
winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="300"}'
winrm set winrm/config '@{MaxTimeoutms="1800000"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/service/auth '@{Basic="true"}'

netsh advfirewall firewall add rule name="WinRM 5985" protocol=TCP dir=in localport=5985 action=allow
netsh advfirewall firewall add rule name="WinRM 5986" protocol=TCP dir=in localport=5986 action=allow
netsh advfirewall firewall add rule name="RDP" protocol=TCP dir=in localport=3389 action=allow
netsh advfirewall firewall add rule name="http" protocol=TCP dir=in localport=80 action=allow

net stop winrm
sc.exe config winrm start=auto
net start winrm
 
Install-WindowsFeature -name Web-Server -IncludeManagementTools
</powershell>
EOF
 
 
vpc_security_group_ids=[aws_security_group.demo.id]
 
}


resource "aws_instance" "vm2" {
  ami           = data.aws_ami.windows-ami.image_id
  instance_type = "t2.micro"

 subnet_id = aws_subnet.main-public-2.id
 user_data     = <<EOF
<powershell>
net user ${var.INSTANCE_USERNAME} '${var.INSTANCE_PASSWORD}' /add /y
net localgroup administrators ${var.INSTANCE_USERNAME} /add

winrm quickconfig -q
winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="300"}'
winrm set winrm/config '@{MaxTimeoutms="1800000"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/service/auth '@{Basic="true"}'

netsh advfirewall firewall add rule name="WinRM 5985" protocol=TCP dir=in localport=5985 action=allow
netsh advfirewall firewall add rule name="WinRM 5986" protocol=TCP dir=in localport=5986 action=allow
netsh advfirewall firewall add rule name="RDP" protocol=TCP dir=in localport=3389 action=allow
netsh advfirewall firewall add rule name="http" protocol=TCP dir=in localport=80 action=allow

net stop winrm
sc.exe config winrm start=auto
net start winrm


Install-WindowsFeature -name Web-Server -IncludeManagementTools
</powershell>
EOF


vpc_security_group_ids=[aws_security_group.demo.id]

}




output "instance_vm1_ip" {
  value = aws_instance.vm1.public_ip
}
output "instance_vm2_ip" {
  value = aws_instance.vm2.public_ip
}

output "instance_vm1_dns" {
  value = aws_instance.vm1.public_dns
}

output "instance_vm2_dns" {
  value = aws_instance.vm2.public_dns
}

output "nlb_dns_name" {
  value = aws_lb.test.dns_name
}


