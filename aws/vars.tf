variable "AWS_ACCESS_KEY" {

}

variable "AWS_SECRET_KEY" {

}

variable "AWS_REGION" {
  default = "eu-central-1"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "/root/.ssh/id_rsa"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "/root/.ssh/id_rsa.pub"
}

variable "INSTANCE_USERNAME" {
  default = "demo"
}

variable "INSTANCE_PASSWORD" {
default = "qwe_334455"
}

variable "INSTANCE_COUNT" {
default = "2"

}
